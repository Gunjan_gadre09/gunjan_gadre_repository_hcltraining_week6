create database week6_gunjan;

create table favoritemovies(id int,title varchar(20),year int,storyline varchar(100),geners varchar(20),rating int);

insert into favoritemovies values(1,'3 Idiots','2006','Farhan Qureshi and Raju Rastogi with their fellow collegian','Comedy',8);

insert into favoritemovies values(2,'Chupke Chupke','1975','Raghav brags thatcan smell a rat or an impersonatorautomatically','Romance',7);

insert into favoritemovies values(3,'A Wednesday','2008','A man calls up the Mumbai policeand tells them he has placed different','Mystery',9);



create table topratedindian(id int,title varchar(20),year int,storyline varchar(100),geners varchar(20),rating int);

insert into topratedindian values(1,'Sholay','1975','Sholay whos family was killed by a bandit named Gabbar Singh decides to fight fire with fire ','Drama',7);

insert into topratedindian values(2,'Rang De Basanti','2006','A English filmmaker Sue arrives in India to make a film ','Biography',7);

insert into topratedindian values(3,'Talvar','2015','The story revolves around the mysterious murder cases of a 14 year girl','Biography',8);


create table topratedmovies(id int,title varchar(20),year int,storyline varchar(100),geners varchar(20),rating int);

insert into topratedmovies values(1,'Anand','1986','A melodramatic tale of a man with a terminal disease','Drama',6);

insert into topratedmovies values(2,'Dangal','2016','Biopic of Mahavir Singh Phogat who taught wrestling to his daughters ','Biography',7);

insert into topratedmovies values(3,'Samson','2013','A Hebrew with an unusual gift of strength must respond','Crime',8);

insert into topratedmovies values(4,'bazigar','1993','Widowed Madan Chopra lives a very wealthy lifestyle with two daughters','musical',8);



create table moviesinthreater(id int,title varchar(20),year int,storyline varchar(100),geners varchar(20),rating int);

insert into moviesinthreater values(1,'black panthar','2018','After the events of Captain America Civil War','Adventure',7);

insert into moviesinthreater values(2,'Aiyaary','2018','Two officers with patriotic hearts suddenly have a fallout','Crime',7);

insert into moviesinthreater values(3,'Samson','2013','A Hebrew with an unusual gift of strength must respond','Crime',8);



create table moviescoming(id int,title varchar(20),year int,storyline varchar(100),geners varchar(20),rating int);

insert into moviescoming values(1,'Game night','2018','A group of friends meet for game nights','Action',3); 

insert into moviescoming values(2,'Hannah','2017','Intimate portrait of a woman drifting between reality and denial','Drama',5);

insert into moviescoming values(3,'The Lodgers','2017','1920 rural Ireland. Anglo Irish twins Rachel and Edward ','Horrar',5);

insert into moviescoming values(4,'The Chamber','2016','A claustrophobic survival thriller set beneath the Yellow sea off','Thriller',2);




