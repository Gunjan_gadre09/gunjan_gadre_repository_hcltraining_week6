package com.grtlearning.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.grtlearning.bean.MoviesComing;
import com.grtlearning.bean.TopRatedMovies;
import com.grtlearning.resource.DbResource;

public class TopRatedMoviesDao {
	public List<TopRatedMovies> retriveAllMovies() {
		List<TopRatedMovies> listOfMovies = new ArrayList<>();
		try {
			Connection con = DbResource.getDbConnection();
			PreparedStatement pstmt = con.prepareStatement("select * from topratedmovies");
			ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
				TopRatedMovies mt = new TopRatedMovies();
				mt.setId(rs.getInt(1));
				mt.setTitle(rs.getString(2));
				mt.setYear(rs.getInt(3));
				mt.setStoryline(rs.getString(4));
				mt.setGeners(rs.getString(5));
				mt.setRating(rs.getString(6));
				listOfMovies.add(mt);
			}
			} catch (Exception e) {
				System.out.println("In retriveAllMovies method "+e);
			}
		return listOfMovies;
	}
}
