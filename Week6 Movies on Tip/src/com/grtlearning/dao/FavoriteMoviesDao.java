package com.grtlearning.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.grtlearning.bean.FavoriteMovies;
import com.grtlearning.bean.MoviesComing;
import com.grtlearning.resource.DbResource;

public class FavoriteMoviesDao {
	public List<FavoriteMovies> retriveAllFavMovies() {
		List<FavoriteMovies> listOfMovies = new ArrayList<>();
		try {
			Connection con = DbResource.getDbConnection();
			PreparedStatement pstmt = con.prepareStatement("select * from Favoritemovies");
			ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
					FavoriteMovies mt = new FavoriteMovies();
					mt.setId(rs.getInt(1));
					mt.setTitle(rs.getString(2));
					mt.setYear(rs.getInt(3));
					mt.setStoryline(rs.getString(4));
					mt.setGeners(rs.getString(5));
					mt.setRating(rs.getString(6));
					listOfMovies.add(mt);
					//return mt;
			}
			} catch (Exception e) {
				System.out.println("In rretriveAllFavMovies method "+e);
			}
		return listOfMovies;
	}
}
