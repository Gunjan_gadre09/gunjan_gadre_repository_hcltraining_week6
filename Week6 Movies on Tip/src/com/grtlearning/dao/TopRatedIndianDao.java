package com.grtlearning.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.grtlearning.bean.MoviesComing;
import com.grtlearning.bean.TopRatedIndian;
import com.grtlearning.resource.DbResource;

public class TopRatedIndianDao {
	public List<TopRatedIndian> retriveAllMovies() {
		List<TopRatedIndian> listOfMovies = new ArrayList<>();
		try {
			Connection con = DbResource.getDbConnection();
			PreparedStatement pstmt = con.prepareStatement("select * from topratedindian");
			ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
				TopRatedIndian mt = new TopRatedIndian();
				mt.setId(rs.getInt(1));
				mt.setTitle(rs.getString(2));
				mt.setYear(rs.getInt(3));
				mt.setStoryline(rs.getString(4));
				mt.setGeners(rs.getString(5));
				mt.setRating(rs.getString(6));
				listOfMovies.add(mt);
			}
			} catch (Exception e) {
				System.out.println("In retriveAllMovies method "+e);
			}
		return listOfMovies;
	}
}
