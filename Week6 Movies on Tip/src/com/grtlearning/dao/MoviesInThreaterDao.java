package com.grtlearning.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.grtlearning.bean.MoviesComing;
import com.grtlearning.bean.MoviesInThreater;
import com.grtlearning.resource.DbResource;

public class MoviesInThreaterDao {
	public List<MoviesInThreater> retriveAllMoviesInthreater() {
		List<MoviesInThreater> listOfMovies = new ArrayList<>();
		try {
			Connection con = DbResource.getDbConnection();
			PreparedStatement pstmt = con.prepareStatement("select * from Moviesinthreater");
			ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
				MoviesInThreater mt = new MoviesInThreater();
				mt.setId(rs.getInt(1));
				mt.setTitle(rs.getString(2));
				mt.setYear(rs.getInt(3));
				mt.setStoryline(rs.getString(4));
				mt.setGeners(rs.getString(5));
				mt.setRating(rs.getString(6));
				listOfMovies.add(mt);
			}
			} catch (Exception e) {
				System.out.println("In retriveAllMoviesinthreater method "+e);
			}
		return listOfMovies;
	}

}
