package com.grtlearning.test;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import com.grtlearning.bean.TopRatedMovies;
import com.grtlearning.dao.TopRatedMoviesDao;

public class TopRatedMoviesTest {
	TopRatedMoviesDao es = new TopRatedMoviesDao();
	List<TopRatedMovies> listOfmovies = es.retriveAllMovies();
	@Test
	public void testGetId() {
		//fail("Not yet implemented");
		TopRatedMovies movie = listOfmovies.get(3);
		assertEquals(4, movie.getId());
	}

	@Test
	public void testGetTitle() {
		//fail("Not yet implemented");
		TopRatedMovies movie = listOfmovies.get(3);
		assertEquals("bazigar", movie.getTitle());
	
	}

	@Test
	public void testGetYear() {
		//fail("Not yet implemented");
		TopRatedMovies movie = listOfmovies.get(3);
		assertEquals(1993, movie.getYear());
	
	}

	@Test
	public void testGetStoryline() {
		//fail("Not yet implemented");
		TopRatedMovies movie = listOfmovies.get(3);
		assertEquals("Widowed Madan Chopra lives a very wealthy lifestyle with two daughters", movie.getStoryline());
	
	}

	@Test
	public void testGetGeners() {
		//fail("Not yet implemented");
		TopRatedMovies movie = listOfmovies.get(3);
		assertEquals("musical", movie.getGeners());
	
	}

	@Test
	public void testGetRating() {
		//fail("Not yet implemented");
		TopRatedMovies movie = listOfmovies.get(3);
		assertEquals("8", movie.getRating());
	
	}

}
