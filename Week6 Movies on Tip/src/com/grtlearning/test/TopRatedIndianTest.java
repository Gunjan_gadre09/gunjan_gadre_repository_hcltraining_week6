package com.grtlearning.test;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import com.grtlearning.bean.TopRatedIndian;
import com.grtlearning.dao.TopRatedIndianDao;

public class TopRatedIndianTest {
	TopRatedIndianDao es = new TopRatedIndianDao();
	List<TopRatedIndian> listOfmovies = es.retriveAllMovies();
	@Test
	public void testGetId() {
		//fail("Not yet implemented");
		TopRatedIndian movie = listOfmovies.get(2);
		assertEquals(3, movie.getId());

	}

	@Test
	public void testGetTitle() {
		//fail("Not yet implemented");
		TopRatedIndian movie = listOfmovies.get(2);
		assertEquals("Talvar", movie.getTitle());

	}

	@Test
	public void testGetYear() {
		//fail("Not yet implemented")TopRatedIndian movie = listOfmovies.get(2);
		TopRatedIndian movie = listOfmovies.get(2);
		assertEquals(2015, movie.getYear());
	}

	@Test
	public void testGetStoryline() {
		//fail("Not yet implemented");
		TopRatedIndian movie = listOfmovies.get(2);
		assertEquals("The story revolves around the mysterious murder cases of a 14 year girl", movie.getStoryline());
	
	}

	@Test
	public void testGetGeners() {
		//fail("Not yet implemented");
		TopRatedIndian movie = listOfmovies.get(2);
		assertEquals("Biography", movie.getGeners());

	}

	@Test
	public void testGetRating() {
		//fail("Not yet implemented");
		TopRatedIndian movie = listOfmovies.get(2);
		assertEquals("8", movie.getRating());
	}

}
