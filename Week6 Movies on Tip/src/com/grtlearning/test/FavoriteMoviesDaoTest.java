package com.grtlearning.test;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import com.grtlearning.bean.FavoriteMovies;
import com.grtlearning.dao.FavoriteMoviesDao;

public class FavoriteMoviesDaoTest {

	@Test
	public void testRetriveAllFavMovies() {
		//fail("Not yet implemented");
		FavoriteMoviesDao es = new FavoriteMoviesDao();
		List<FavoriteMovies> listOfmovies = es.retriveAllFavMovies();
		assertEquals(3, listOfmovies.size());
		FavoriteMovies movie = listOfmovies.get(0);
		assertEquals(1, movie.getId());
		assertEquals("3 Idiots", movie.getTitle());
		assertEquals(2006, movie.getYear());
		assertEquals("Farhan Qureshi and Raju Rastogi with their fellow collegian", movie.getStoryline());
		assertEquals("Comedy", movie.getGeners());
		assertEquals("8", movie.getRating());
	}

}
