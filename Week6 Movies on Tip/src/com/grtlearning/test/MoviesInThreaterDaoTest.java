package com.grtlearning.test;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;
import com.grtlearning.bean.MoviesInThreater;
import com.grtlearning.dao.MoviesInThreaterDao;

public class MoviesInThreaterDaoTest {

	@Test
	public void testRetriveAllMoviesInthreater() {
		//fail("Not yet implemented");
		MoviesInThreaterDao es = new MoviesInThreaterDao();
		List<MoviesInThreater> listOfmovies = es.retriveAllMoviesInthreater();
		assertEquals(3, listOfmovies.size());
		MoviesInThreater movie = listOfmovies.get(1);
		assertEquals(2, movie.getId());
		assertEquals("Aiyaary", movie.getTitle());
		assertEquals(2018, movie.getYear());
		assertEquals("Two officers with patriotic hearts suddenly have a fallout", movie.getStoryline());
		assertEquals("Crime", movie.getGeners());
		assertEquals("7", movie.getRating());
	}

}
