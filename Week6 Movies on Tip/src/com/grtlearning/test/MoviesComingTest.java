package com.grtlearning.test;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import com.grtlearning.bean.MoviesComing;
import com.grtlearning.dao.MoviesComingDao;

public class MoviesComingTest {

	MoviesComingDao es = new MoviesComingDao();
	List<MoviesComing> listOfmovies = es.retriveAllMovies();

	@Test
	public void testGetId() {
		//fail("Not yet implemented");
		MoviesComing movie = listOfmovies.get(0);
		assertEquals(1, movie.getId());
	}

	@Test
	public void testGetTitle() {
		//fail("Not yet implemented");
		MoviesComing movie = listOfmovies.get(0);
		assertEquals("Game night", movie.getTitle());

	}

	@Test
	public void testGetYear() {
		//fail("Not yet implemented");
		MoviesComing movie = listOfmovies.get(0);
		assertEquals(2018, movie.getYear());

	}

	@Test
	public void testGetStoryline() {
		//fail("Not yet implemented");
		MoviesComing movie = listOfmovies.get(0);
		assertEquals("A group of friends meet for game nights", movie.getStoryline());

	}

	@Test
	public void testGetGeners() {
		//fail("Not yet implemented");
		MoviesComing movie = listOfmovies.get(0);
		assertEquals(1, movie.getId());
		assertEquals("Action", movie.getGeners());

	}

	@Test
	public void testGetRating() {
		//fail("Not yet implemented");
		MoviesComing movie = listOfmovies.get(0);
		assertEquals("3", movie.getRating());

	}

}
