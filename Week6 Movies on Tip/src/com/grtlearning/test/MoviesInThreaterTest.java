package com.grtlearning.test;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import com.grtlearning.bean.MoviesInThreater;
import com.grtlearning.dao.MoviesInThreaterDao;

public class MoviesInThreaterTest {
	MoviesInThreaterDao es = new MoviesInThreaterDao();
	List<MoviesInThreater> listOfmovies = es.retriveAllMoviesInthreater();
	
	@Test
	public void testGetId() {
		//fail("Not yet implemented");
		MoviesInThreater movie = listOfmovies.get(1);
		assertEquals(2, movie.getId());
	}

	@Test
	public void testGetTitle() {
		//fail("Not yet implemented");
		MoviesInThreater movie = listOfmovies.get(1);
		assertEquals("Aiyaary", movie.getTitle());
	}

	@Test
	public void testGetYear() {
		//fail("Not yet implemented");
		MoviesInThreater movie = listOfmovies.get(1);
		assertEquals(2018, movie.getYear());
		}

	@Test
	public void testGetStoryline() {
		//fail("Not yet implemented");
		MoviesInThreater movie = listOfmovies.get(1);
		assertEquals("Two officers with patriotic hearts suddenly have a fallout", movie.getStoryline());
	}

	@Test
	public void testGetGeners() {
		//fail("Not yet implemented");
		MoviesInThreater movie = listOfmovies.get(1);
		assertEquals("Crime", movie.getGeners());
	}

	@Test
	public void testGetRating() {
		//fail("Not yet implemented");
		MoviesInThreater movie = listOfmovies.get(1);
		assertEquals("7", movie.getRating());
	}

}
