package com.grtlearning.test;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import com.grtlearning.bean.FavoriteMovies;
import com.grtlearning.dao.FavoriteMoviesDao;

public class FavoriteMoviesTest {
	FavoriteMoviesDao es = new FavoriteMoviesDao();
	List<FavoriteMovies> listOfmovies = es.retriveAllFavMovies();

	@Test
	public void testGetId() {
		//fail("Not yet implemented");
		FavoriteMovies movie = listOfmovies.get(0);
		assertEquals(1, movie.getId());
	}

	@Test
	public void testGetTitle() {
		//fail("Not yet implemented");
		FavoriteMovies movie = listOfmovies.get(0);
		assertEquals("3 Idiots", movie.getTitle());
	}

	@Test
	public void testGetYear() {
		//fail("Not yet implemented");
		FavoriteMovies movie = listOfmovies.get(0);
		assertEquals(2006, movie.getYear());
	}

	@Test
	public void testGetStoryline() {
		//fail("Not yet implemented");
		FavoriteMovies movie = listOfmovies.get(0);
		assertEquals("Farhan Qureshi and Raju Rastogi with their fellow collegian", movie.getStoryline());
		
	}

	@Test
	public void testGetGeners() {
		//fail("Not yet implemented");
		FavoriteMovies movie = listOfmovies.get(0);
		assertEquals("Comedy", movie.getGeners());
	}

	@Test
	public void testGetRating() {
		//fail("Not yet implemented");
		FavoriteMovies movie = listOfmovies.get(0);
		assertEquals("8", movie.getRating());
	}

}
