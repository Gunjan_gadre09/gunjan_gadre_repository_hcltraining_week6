package com.grtlearning.test;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import com.grtlearning.bean.TopRatedIndian;
import com.grtlearning.dao.TopRatedIndianDao;

public class TopRatedIndianDaoTest {

	@Test
	public void testRetriveAllMovies() {
		//fail("Not yet implemented");
		TopRatedIndianDao es = new TopRatedIndianDao();
		List<TopRatedIndian> listOfmovies = es.retriveAllMovies();
		//assertEquals(3, listOfmovies.size());
		TopRatedIndian movie = listOfmovies.get(2);
		assertEquals(3, movie.getId());
		assertEquals("Talvar", movie.getTitle());
		assertEquals(2015, movie.getYear());
		assertEquals("The story revolves around the mysterious murder cases of a 14 year girl", movie.getStoryline());
		assertEquals("Biography", movie.getGeners());
		assertEquals("8", movie.getRating());
	}

}
