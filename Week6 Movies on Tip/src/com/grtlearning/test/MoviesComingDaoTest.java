package com.grtlearning.test;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import com.grtlearning.bean.MoviesComing;
import com.grtlearning.dao.MoviesComingDao;

public class MoviesComingDaoTest {

	@Test
	public void testRetriveAllComingMovies() {
		//fail("Not yet implemented");
		MoviesComingDao es = new MoviesComingDao();
		List<MoviesComing> listOfmovies = es.retriveAllMovies();
		assertEquals(4, listOfmovies.size());
		MoviesComing movie = listOfmovies.get(0);
		assertEquals(1, movie.getId());
		assertEquals("Game night", movie.getTitle());
		assertEquals(2018, movie.getYear());
		assertEquals("A group of friends meet for game nights", movie.getStoryline());
		assertEquals("Action", movie.getGeners());
		assertEquals("3", movie.getRating());

	}

}
