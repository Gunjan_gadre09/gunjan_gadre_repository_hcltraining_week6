package com.grtlearning.test;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;
import com.grtlearning.bean.TopRatedMovies;
import com.grtlearning.dao.TopRatedMoviesDao;

public class TopRatedMoviesDaoTest {

	@Test
	public void test() {
		//fail("Not yet implemented");
		TopRatedMoviesDao es = new TopRatedMoviesDao();
		List<TopRatedMovies> listOfmovies = es.retriveAllMovies();
		assertEquals(4, listOfmovies.size());
		TopRatedMovies movie = listOfmovies.get(3);
		assertEquals(4, movie.getId());
		assertEquals("bazigar", movie.getTitle());
		assertEquals(1993, movie.getYear());
		assertEquals("Widowed Madan Chopra lives a very wealthy lifestyle with two daughters", movie.getStoryline());
		assertEquals("musical", movie.getGeners());
		assertEquals("8", movie.getRating());
	}

}
