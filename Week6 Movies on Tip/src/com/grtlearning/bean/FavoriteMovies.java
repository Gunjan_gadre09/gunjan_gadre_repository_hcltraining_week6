package com.grtlearning.bean;

public class FavoriteMovies implements Movies{
	private int id;
	private String title;
	private int year;
	private String storyline;
	private String geners;
	private String rating;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public String getStoryline() {
		return storyline;
	}
	public void setStoryline(String storyline) {
		this.storyline = storyline;
	}
	public String getGeners() {
		return geners;
	}
	public void setGeners(String geners) {
		this.geners = geners;
	}
	public String getRating() {
		return rating;
	}
	public void setRating(String rating) {
		this.rating = rating;
	}
	public FavoriteMovies(int id, String title, int year, String storyline, String geners, String rating) {
		super();
		this.id = id;
		this.title = title;
		this.year = year;
		this.storyline = storyline;
		this.geners = geners;
		this.rating = rating;
	}
	public FavoriteMovies() {
		super();
		// TODO Auto-generated constructor stub
	}
	@Override
	public String toString() {
		return "MoviesComing [id=" + id + ", title=" + title + ", year=" + year + ", storyline=" + storyline
				+ ", geners=" + geners + ", rating=" + rating + "]";
	}
	
}
